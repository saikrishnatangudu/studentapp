package com.hcl.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.Exception.DataNotFoundException;
import com.hcl.Exception.StudentNotFoundException;
import com.hcl.model.Student;
import com.hcl.repositary.StudentRepositary;

import net.bytebuddy.implementation.bytecode.Throw;

@Service
public class StudentServiceImpl implements StudentSevice {
	@Autowired
	private StudentRepositary studentRepositary;

	public void saveStudent(Student student) {

		studentRepositary.save(student);

	}

	public Student getStudent(int id) {

		return studentRepositary.findById(id).orElseThrow(()-> new StudentNotFoundException(id));

	}

	public void deleteStudent(int id) {

		studentRepositary.deleteById(id);
		

	}

	public List<Student> getAllStudents() {

		List<Student> students=	studentRepositary.findAll();
		
		if (students.isEmpty()) {
			throw new DataNotFoundException();
		}
		return students;
	}




	@Override
	public void updateStudent(Student student) {

		studentRepositary.save(student);
	}

}
