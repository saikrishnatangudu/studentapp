package com.hcl.repositary;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.model.Student;
@Repository
public interface StudentRepositary extends JpaRepository<Student, Integer> {

}
